# Introdução {#sec:introduction}

A **camada de transporte** é responsável pela movimentação de dados, de forma eficiente e confiável, entre processos em execução nos equipamentos conectados a uma rede de computadores. Esta camada é ainda responsável pelo controlo de fluxo e pelo controlo de congestão.

Pelo facto desta camada desempenhar funções tão importantes torna-se necessário o conhecimento e entendimento dos protocolos que esta configura. Assim, temos: o protocolo UDP, que não é orientado à conexão e temos o TCP que é confiável e orientado à conexão e que configura, ainda, o controlo de congestão.

A **camada de transporte** disponibiliza uma _ligação lógica_ entre aplicações que estão a ser executadas em Sistemas Terminais diferentes. Os protocolos mencionados são executados nos Sistemas Terminais (O sistema emissor parte a mensagem gerada pela aplicação em _segmentos_ que passa à camada de transporte. Por outro lado, o sistema recetor junta os diferentes **segmentos** que constituem uma mensagem que passa à respetiva aplicação).
