# Questões e Respostas {#sec:questions_answers}

## Questão 1

**Inclua no relatório uma tabela em que identifique, para cada comando executado, qual o protocolo de aplicação, o protocolo de transporte, porta de atendimento e _overhead_ de transporte.**

| Comando | Protocolo de aplicação | Protocolo de transporte | Porta de atendimento | Overhead de transporte |
|-|-|-|-|-|
| browser/http | SSL | TCP | 443 | 20/233 (8.58%) |
| ftp | FTP | TCP | 21 | 20/46 (43.48%) | (resposta)
| tftp | TFTP | UDP | 69 | 8/52 (15.39%) |
| telnet | Telnet | TCP | 23 | 20/47 (42.55%) |
| ssh | ssh | TCP | 46887 | 20/61 (32.79%) |
| nslookup | DNS | UDP | 53 | 8/39 (20.51%) |
| traceroute |-| UDP | 33434 | 8/40 (20.00%) |

## Questão 2

**Uma representação num diagrama temporal das transferências da _file1_ por _FTP_ e _TFTP_ respetivamente. Se for caso disso, identifique as fases de estabelecimento de conexão, transferência de dados e fim de conexão. Identifique também claramente os tipos de segmentos trocados e os números de sequência usados quer nos dados como nas confirmações.**

![Captura _Wireshark_ da transferência da **file1** por **FTP**](figures/wireshark_ftp.png)

![Diagrama da transferência da **file1** for **FTP**](figures/cc_ftp.png){ height=600px }

![Captura _Wireshark_ da transferência da **file1** por **FTP**](figures/wireshark_tftp.png)

![Diagrama da transferência da **file1** por **TFTP**](figures/tftp.png){ height=400px }

## Questão 3

**Com base nas experiências realizadas, distinga e compare sucintamente as quatroaplicações de transferência de ficheiros que usou nos seguintes pontos (i) uso da camada de transporte; (ii) eficiência na transferência; (iii) complexidade; (iv) segurança;**

O SFTP (SSH File Transfer Protocol) é um protocolo seguro que requer autenticação. Este protocolo, ao nível da camada de transporte, utiliza TCP. A eficiência na transferência não é a maior, uma vez que estes se encontram fragmentados e encriptados, o que implica tempo do lado do recetor para reconstruir o pacote inicial. É, na nossa opinião, o mais complexo protocolo de transferência dos que estudamos, visto que no início da conexão são trocadas chaves para que seja possível a comunicação entre cliente e servidor ainda que as mensagens sejam cifradas. Os pacotes de dados são ainda fragmentados. É um protocolo seguro uma vez que as mensagens são cifradas e é estabelecida, no início da sessão, uma chave.

O FTP (File Transfer Protocol) é um protocolo que requer autenticação com o servidor. Ao nível da camada de transporte utiliza TCP, que é orientado à conexão. Na transferência é eficiente uma vez que o ficheiro é transferido em binário, num único pacote. Ao nível de segurança este protocolo não é dos melhores uma vez que, por exemplo, transmite as credenciais de acesso em texto puro e o ficheiro é transferido em binário.

O TFTP (Trivial File Transfer Protocol) é um protocolo extremamente simples. Ao nível da camada de transporte utiliza UDP, que é orientado ao pacote. É extremamente eficiente em virtude do muito reduzido _overhead_ (todos os pacotes que são trocados dizem apenas respeito à transferência do ficheiro, isto é: um pedido de transferência, o próprio ficheiro e o _acknowledgement_ da receção do ficheiro). Como consequência, é também muito simples na transferência. Não implementa segurança adicional uma vez que nem sequer exige o estabelecimento de uma sessão e o pacote que contém o ficheiro é enviado em binário, sem qualquer tipo de cifra.

O HTTP (Hypertext Transfer Protocol) é um protocolo simples. Ao nível da camada de transporte utiliza TCP. A eficiência na transferência é grande, visto que não tem um grande overhead em termos de troca de mensagens relativamente à transferência. A complexidade é reduzida visto que o ficheiro é transferido em texto puro e apenas exige o estabelecimento de uma conexão simples. Em termos de segurançã o protocolo falha pelo facto de o ficheiro a transferir ser comunicado em texto puro, sendo por isso suscetível de ser intercetado. É ainda uma falha de segurança (dependendo do esquema de uso) o facto de não ser necessário estabelecer uma sessão, o que o torna os ficheiros presentens no servidor acessíveis a qualquer usuário que saiba o seu endereço.

## Questão 4

**As  características  das  ligações  de  rede  têm  uma  enorme  influência  nos  níveis  de Transporte  e  de  Aplicação.  Discuta, relacionando a resposta com as experiências realizadas, as influências das situações de perda ou duplicação de pacotes IP no desempenho global de Aplicações fiáveis (se possível, relacionando com alguns dos mecanismos de transporte envolvidos).**

O protocolo utilizado ao nível de transporte tem grande impacto no desempenho da aplicação. Temos duas grandes opções ao nível da camada de transporte: TCP e UDP. O TCP é orientado à conexão, ou seja, o seu objetivo principal é a fiabilidade e correção na transmissão de pacotes, isto é: o TCP certifica-se que cada pacote chega ao destino e é reconhecido pela mesma e certifica-se que os pacotes chegam lá também na ordem correta. No extremo oposto temos o UDP que é orientado ao pacote, ou seja, o seu objetivo é transmitir os pacotes com rapidez sendo que não espera por nenhuma espécie de reconhecimento do destino e nem tem especial cuidado com a ordem pela qual os pacotes são enviados.
No caso da conexão à rede não ser a ideal provavelmente obter-se-ão melhores resultados com UDP do que com TCP, uma vez que o TCP estaria constantemente a tentar retransmitir pacotes cujo _acknowledgement_ não chegou dentro da janela temporal pretendida, acrescentando, deste modo, tempo de espera na aplico. No caso do UDP a aplicação iria receber os pacotes possíveis e não desperdiçaria recursos a tentar retransmitir pacotes que não foram reconhecidos. No entanto, depende do tipo de aplicação: se objetivo for transferir corretamente um ficheiro vamos querer sempre TCP para que se certifique que o ficheiro é transferido integral e corretamente.
