# Conclusão {#sec:conclusion}

Este trabalho prático é um complemento valioso e permitiu-nos ver em funcionamento conceitos e procotolos lecionados nas aulas teóricas. Permitiu-nos expandir conhecimentos ao nível dos protocolos de transporte (UDP e TCP) e ao nível dos protocolos aplicacionais (SFTP, FTP, TFTP e HTTP). Utilizamos a ferramenta _Wireshark_ para inspecionar os datagramas e o seu conteúdo.

A transferência de ficheiros utilizando os diferentes protocolos aplicacionais, a captura dos datagramas e a análise dos mesmos tornou evidentes as suas diferenças: a nível de complexidade, eficiência e segurança.
